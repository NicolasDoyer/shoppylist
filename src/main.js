import Vue from 'vue';
import Ant from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
import VueRouter from 'vue-router'
import App from './App.vue';
import Home from "@/components/pages/Home";
import Error404 from "@/components/pages/Error404";
import MyLists from "@/components/pages/MyLists";
import NewList from "@/components/pages/NewList";
import { Button } from 'ant-design-vue';
import ViewList from "@/components/pages/ViewList";
import NewItem from "@/components/pages/NewItem";

Vue.config.productionTip = false;
Vue.use(Ant);
Vue.use(VueRouter);
Vue.use(Button);

const routes = [
    {
        path: '/',
        meta: {
            title: 'ShoppyList',
        },
        component: Home
    },
    {
        path: '/my-lists',
        meta: {
            title: 'ShoppyList - My lists',
        },
        component: MyLists
    },
    {
        path: '/new-list',
        meta: {
            title: 'ShoppyList - New list',
        },
        component: NewList
    },
    {
        path: '/list/:id',
        component: ViewList,
        props: true
    },
    {
        path: '/list/:id/add',
        component: NewItem,
        props: true
    },
    {
        path: '*',
        meta: {
            title: 'ShoppyList - Page not found',
        },
        component: Error404
    },
];

const router = new VueRouter({
    mode: 'history',
    routes
});

// Change page title in head tag
router.beforeEach((to, from, next) => {
    const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);
    if (nearestWithTitle) document.title = nearestWithTitle.meta.title;
    next();
});

new Vue({
    router,
    render: h => h(App),
}).$mount('#app');
