/** STORAGE **/
const AppStorage = {

    state: {
        currentTitle: 'Homepage'
    },

    setCurrentTitle: (title) => {
        AppStorage.state.currentTitle = title;
    }

};

export default AppStorage;