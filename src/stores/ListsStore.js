/** STORAGE **/
const LISTS_STORAGE_KEY = 'LISTS';
const ListsStorage = {

    state: {
        lists: JSON.parse(localStorage.getItem(LISTS_STORAGE_KEY)) || []
    },

    save: () => {
        localStorage.setItem(LISTS_STORAGE_KEY, JSON.stringify(ListsStorage.state.lists));
    },

    deleteList: (key) => {
        const indexToDelete = ListsStorage.state.lists.findIndex(list => list.key === parseInt(key));
        ListsStorage.state.lists.splice(indexToDelete,1);
        ListsStorage.save();
    },

    deleteListItem: (listKey, itemKey) => {
        const targetedList = ListsStorage.getList(listKey);
        const indexToDelete = targetedList.items.findIndex(item => item.key === parseInt(itemKey));
        targetedList.items.splice(indexToDelete,1);
        ListsStorage.save();
    },

    getList: (key) => {
        const index = ListsStorage.state.lists.findIndex(list => list.key === parseInt(key));
        return ListsStorage.state.lists[index];
    },

    getListItem: (listKey, itemKey) => {
        const targetedList = ListsStorage.getList(listKey);
        const index = targetedList.items.findIndex(item => item.key === parseInt(itemKey));
        return targetedList.items[index];
    },

    nextKey: () => {
        const data = ListsStorage.state.lists;
        return data.reduce((max, todo) => Math.max(max, todo.key), -1) + 1;
    },

    nextItemKey: (key) => {
        const targetedList = ListsStorage.getList(key);
        if(targetedList) {
            return targetedList.items.reduce((max, todo) => Math.max(max, todo.key), -1) + 1;
        } else {
            return 0;
        }
    },

    addList: (title, description, image) => {
        const nextKey = ListsStorage.nextKey();
        ListsStorage.state.lists.push(
            {key: nextKey, title: title, description: description, image: image, items: []}
        );
        ListsStorage.save();
    },

    addListItem: (key, title, quantity, image) => {
        const nextItemKey = ListsStorage.nextItemKey(key);
        const updatedList = ListsStorage.getList(key);
        updatedList.items.push(
            {key: nextItemKey, title: title, quantity: quantity, image: image}
        );
        ListsStorage.save();
    }
};

export default ListsStorage;